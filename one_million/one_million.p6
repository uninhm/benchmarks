#!/usr/bin/env raku

for 1..1000000 -> $n {
	print "Hello, this is iteration number: $n\n"
}
